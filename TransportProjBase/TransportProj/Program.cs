﻿﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            int CityLength = 10;
            int CityWidth = 10;
            //int BuildingCount = 40;

            City MyCity = new City(CityLength, CityWidth);
            //Building[] buildings = MyCity.AddBuildingsToCity(BuildingCount);

            //Graph graph = new Graph(MyCity.Map);
            //graph.Draw();

            CarFactory factory = new SedanFactory();

            Car car = MyCity.AddCarToCity(factory, rand.Next(CityLength), rand.Next(CityWidth));
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength), rand.Next(CityWidth), rand.Next(CityLength), rand.Next(CityWidth));

            //bool bFound = graph.FindPath(car.XPos, car.YPos, passenger.StartingXPos, passenger.StartingYPos);
            //if (bFound)
            //{
            //    Console.WriteLine("The car can reach the passenger");
            //}
            //else
            //{
            //    Console.WriteLine("The car cannot reach the passenger");
            //}
            
            while (!passenger.IsAtDestination())
            {
                MoveAndDownload(car, passenger);
                //Tick(car, passenger);
            }

            Console.ReadLine();
            return;
        }

        /// <summary>
        /// Takes the passenger to the destination and loads the veyo website asynchronously
        /// I assumed that because downlding a website is slower than moving the car, the purpose
        /// of this task is to do these two activities on different threads
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static async void MoveAndDownload(Car car, Passenger passenger)
        {
            WebClient client = new WebClient();
            // Downloads veyo website.
            Task<string> getStringTask = client.DownloadStringTaskAsync(new Uri("http://www.veyo.com/"));

            Tick(car, passenger);

            string urlContents = await getStringTask;

            // Write values.
            Console.WriteLine("veyo.com was downloaded");
        }
        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            if (car == null || passenger == null)
            {
                return;
            }

            if (car.Passenger == null) // Is the passenger in the car?
            {
                //Pick up the passenger if the car reaches the passenger
                if (car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos)
                {
                    passenger.GetInCar(car);
                    return;
                }
                else
                {
                    // Move to the passenger
                    if (car.XPos < passenger.StartingXPos)
                    {
                        car.MoveRight();
                        return;
                    }
                    if (car.XPos > passenger.StartingXPos)
                    {
                        car.MoveLeft();
                        return;
                    }
                    if (car.YPos < passenger.StartingYPos)
                    {
                        car.MoveUp();
                        return;
                    }
                    if (car.YPos > passenger.StartingYPos)
                    {
                        car.MoveDown();
                        return;
                    }
                }
            }
            else
            {
                // Passenger is in the car. Take her to the destination
                if (car.XPos < passenger.DestinationXPos)
                {
                    car.MoveRight();
                    return;
                }
                if (car.XPos > passenger.DestinationXPos)
                {
                    car.MoveLeft();
                    return;
                }
                if (car.YPos < passenger.DestinationYPos)
                {
                    car.MoveUp();
                    return;
                }
                if (car.YPos > passenger.DestinationYPos)
                {
                    car.MoveDown();
                    return;
                }
            }
        }
    }
}
