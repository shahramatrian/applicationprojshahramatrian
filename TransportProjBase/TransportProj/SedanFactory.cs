﻿using System;

namespace TransportProj
{
    /// <summary>
    /// Builds and returns a Sedan
    /// </summary>
    class SedanFactory: CarFactory
    {
        public override Car BuildCar(int xPos, int yPos, City city, Passenger passenger)
        {
            return new Sedan(xPos, yPos, city, passenger);
        }
    }
}
