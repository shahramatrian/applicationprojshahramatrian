﻿using System;

namespace TransportProj
{
    /// <summary>
    /// Responsible for building race cars
    /// </summary>
    class RaceCarFactory:CarFactory
    {
        public override Car BuildCar(int xPos, int yPos, City city, Passenger passenger)
        {
            return new RaceCar(xPos, yPos, city, passenger);
        }
    }
}
