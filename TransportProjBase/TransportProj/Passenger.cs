﻿using System;

namespace TransportProj
{
    public class PositionEventArgs : EventArgs
    {
        public int OldXPos { get; private set; }
        public int OldYPos { get; private set; }
        public int NewXPos { get; private set; }
        public int NewYPos { get; private set; }

        public PositionEventArgs(int xOldPos, int yOldPos, int newXPos, int newYPos)
        {
            this.OldXPos = xOldPos;
            this.OldYPos = yOldPos;
            this.NewXPos = newXPos;
            this.NewYPos = newYPos;
        }
    }

    public class Passenger
    {
        public delegate void PositionChangeHandler(object passenger, PositionEventArgs positionInfo);
        public PositionChangeHandler PositionChanged;

        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; set; }
        public City City { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

        public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            // if anyone has subscribed, notify them (i.e. CityPoistion)
            /*if (PositionChanged != null)
            {
                // if anyone has subscribed, notify them (i.e. CityPoistion)
                PositionChanged(this, new PositionEventArgs(this.StartingXPos, this.StartingYPos, car.XPos, car.YPos));
            }*/

            Console.WriteLine("Passenger got in car.");
        }

        public void GetOutOfCar()
        {
            Car = null;
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }
    }
}
