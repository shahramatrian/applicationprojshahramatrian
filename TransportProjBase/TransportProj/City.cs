﻿
using System;
namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
        public CityPosition Map { get; private set; }
        //public Passenger Passenger { get; private set; } // Added passenger can be accessed

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
            Map = new CityPosition(xMax, yMax);

        }

        public Car AddCarToCity(CarFactory carFactory, int xPos, int yPos)
        {
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            while (Map.Blocks[xPos, yPos] == CityBlock.Building)
            {
                xPos = rand.Next(XMax);
                yPos = rand.Next(YMax);
            }
            Map.Car = carFactory.BuildCar(xPos, yPos, this, null);
            return Map.Car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            // We may use Singleton pattern to assure there would be only one passenger in the city,
            // but it is not necessary now
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            while (Map.Blocks[startXPos, startYPos] == CityBlock.Building)
            {
                startXPos = rand.Next(XMax);
                startYPos = rand.Next(YMax);
            }

            while (Map.Blocks[destXPos, destYPos] == CityBlock.Building)
            {
                destXPos = rand.Next(XMax);
                destYPos = rand.Next(YMax);
            }

            Map.Passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);
            Map.SubscribeToPosChanges();
            return Map.Passenger;
        }


        public Building[] AddBuildingsToCity(int count)
        {
            Building[] buildings = new Building[count];
            Random rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            for (int c = 0; c < count; c++)
            {
                 buildings[c] = new Building(rand.Next(XMax), rand.Next(YMax), this);
            }
            Map.Buildings = buildings;
            return Map.Buildings;
        }
    }
}
