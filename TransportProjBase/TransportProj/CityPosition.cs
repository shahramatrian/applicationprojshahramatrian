﻿using System;

namespace TransportProj
{        
    public enum CityBlock { Vacant, Car, Passenger, Building };
    public class CityPosition
    {
        public CityBlock[,] Blocks
        {
            get { return _blocks; }
        }

        public CityPosition(int iMaxX, int iMaxY)
        {
            // make a two dimentional array of the city size and fill it with vacant tiles
            _blocks = new CityBlock[iMaxX, iMaxY];
            for (int x = 0; x < iMaxX; x++)
                for (int y = 0; y < iMaxY; y++ )
                {
                    _blocks[x, y] = CityBlock.Vacant;
                }
        }

        public Car Car
        {
            get { return _car; }
            set { this._car = value; _blocks[_car.XPos, _car.YPos] = CityBlock.Car; }
        }

        public Building[] Buildings
        {
            get { return _buildings; }
            set { 
                this._buildings = value;
                for (int i = 0; i < _buildings.Length; i++)
                {
                    _blocks[_buildings[i].XPos, _buildings[i].YPos] = CityBlock.Building;
                }
            }
        }

        public Passenger Passenger 
        {
            get { return _passenger;}
            set { 
                this._passenger = value;
                if (_blocks[_passenger.GetCurrentXPos(), _passenger.GetCurrentYPos()] == CityBlock.Vacant)
                {
                    _blocks[_passenger.GetCurrentXPos(), _passenger.GetCurrentYPos()] = CityBlock.Passenger;
                }
            }
        }

        public void SubscribeToPosChanges()
        {
            Car.PositionChanged +=
                new Car.PositionChangeHandler(CarPosChange);

        }

        public void CarPosChange(object theCar, PositionEventArgs position)
        {
            _blocks[position.OldXPos, position.OldYPos] = CityBlock.Vacant;
            if (_blocks[position.NewXPos, position.NewYPos] != CityBlock.Building)
            {
                _blocks[position.NewXPos, position.NewYPos] = CityBlock.Car;
            }
            Console.WriteLine(String.Format("The car moved to x - {0} y - {1}", position.NewXPos, position.NewYPos));
        }

        public void Draw()
        {
            for (int x = 0; x < _blocks.GetLength(0); x++)
                for (int y = 0; y < _blocks.GetLength(1); y++ )
                {
                    Console.Write(String.Format("{0},{1}: {2}, ", x, y, _blocks[x, y]));
                }
            Console.WriteLine();
        }

        private Car _car;
        private Passenger _passenger;
        private CityBlock[,] _blocks;
        private Building[] _buildings;
    }
}
