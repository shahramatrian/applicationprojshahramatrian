﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger)
            : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax -1 && CanMoveFastY())
            {
                MoveUpFast();
            } else if (YPos < City.YMax)
            {
                YPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 1 && CanMoveFastY())
            {
                MoveDownFast();
            } else if (YPos > 0)
            {
                YPos--;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax - 1 && CanMoveFastX())
            {
                MoveRightFast();
            } else if (XPos < City.XMax)
            {
                XPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 1 && CanMoveFastX())
            {
                MoveLeftFast();
            } else if (XPos > 0)
            {
                XPos--;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Race car moved to x - {0} y - {1}", XPos, YPos));
        }


        private void MoveUpFast() {
            YPos += 2;
            WritePositionToConsole();
        }

        private void MoveDownFast() {
            YPos -= 2;
            WritePositionToConsole();
        }

        private void MoveRightFast() {
            XPos += 2;
            WritePositionToConsole();
        }

        private void MoveLeftFast() {
            XPos -= 2;
            WritePositionToConsole();
        }

        /// <summary>
        /// Checks if the car can take two steps right/left
        /// </summary>
        /// <returns> True if the passenger or the destination is at least 2 steps left/right</returns>
        private bool CanMoveFastX()
        {
            if (Passenger == null)
            {
                return (Math.Abs(XPos - City.Map.Passenger.StartingXPos) > 1);
            }
            else
            {
                return (Math.Abs(XPos - City.Map.Passenger.DestinationXPos) > 1);
            }
        }

        /// <summary>
        /// Checks if the car can take two steps up/down
        /// </summary>
        /// <returns> True if the passenger or the destination is at least 2 steps up/down</returns>
        private bool CanMoveFastY()
        {
            if (Passenger == null)
            {
                return (Math.Abs(YPos - City.Map.Passenger.StartingYPos) > 1);
            }
            else
            {
                return (Math.Abs(YPos - City.Map.Passenger.DestinationYPos) > 1);
            }
        }
    }
}
