﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos++;
                if (PositionChanged != null)
                {
                    PositionChanged(this, new PositionEventArgs(XPos, YPos - 1, XPos, YPos));
                    if (Passenger != null && Passenger.PositionChanged != null)
                    {
                        Passenger.PositionChanged(this.Passenger, new PositionEventArgs(XPos, YPos - 1, XPos, YPos));
                    }
                }
                //WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos--;
                if (PositionChanged != null)
                {
                    PositionChanged(this, new PositionEventArgs(XPos, YPos + 1, XPos, YPos));
                    if (Passenger != null && Passenger.PositionChanged != null)
                    {
                        Passenger.PositionChanged(this.Passenger, new PositionEventArgs(XPos, YPos + 1, XPos, YPos));
                    }

                }
                //WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos++;
                if (PositionChanged != null)
                {
                    PositionChanged(this, new PositionEventArgs(XPos - 1, YPos, XPos, YPos));
                    if (Passenger != null && Passenger.PositionChanged != null)
                    {
                        Passenger.PositionChanged(this.Passenger, new PositionEventArgs(XPos - 1, YPos, XPos, YPos));
                    }

                }
                //WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos--;
                if (PositionChanged != null)
                {
                    PositionChanged(this, new PositionEventArgs(XPos+1, YPos, XPos, YPos));
                    if (Passenger != null && Passenger.PositionChanged != null)
                    {
                        Passenger.PositionChanged(this.Passenger, new PositionEventArgs(XPos + 1, YPos, XPos, YPos));
                    }

                }
                //WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
