﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace TransportProj
{
    /// <summary>
    /// A graph made of adjacent linkes lists
    /// </summary>
    public class Graph
    {
        public List<Node> NodeList;

        public Graph()
        {
            NodeList = new List<Node>();
        }

        /// <summary>
        /// Creates a graph from the city map (i.e. roads)
        /// </summary>
        /// <param name="cityMap"></param>
        public Graph(CityPosition cityMap)
        {
            NodeList = new List<Node>();
            NodeMap = new Dictionary<Point, int>();
            RevNodeMap = new Dictionary<int,Point>();

            for (int x = 0; x < City.XMax; x++)
                for (int y = 0; y < City.YMax; y++)
                {
                    if (cityMap.Blocks[x, y] == CityBlock.Vacant)
                    {
                        Node node = new Node(NodeList.Count);
                        Point p = new Point(x, y);

                        // Maps each empty block to a node index and vice versa
                        NodeMap.Add(p, node.Index);
                        RevNodeMap.Add(node.Index, p);
                        NodeList.Add(node);
                    }
                }

            foreach (Node node in NodeList)
            {
                Point p = RevNodeMap[node.Index];
                // TODO: the sequence Right, Left, Up, Down needs to be randomized
                if (LookRight(cityMap.Blocks, p))
                {
                    node.AddNeighbor(NodeMap[new Point(p.X+1, p.Y)]);
                }

                if (LookLeft(cityMap.Blocks, RevNodeMap[node.Index]))
                { 
                    node.AddNeighbor(NodeMap[new Point(p.X -1, p.Y)]); 
                }

                if (LookUp(cityMap.Blocks, p))
                { node.AddNeighbor(NodeMap[new Point(p.X, p.Y+1)]); }

                if (LookDown(cityMap.Blocks, p))
                { node.AddNeighbor(NodeMap[new Point(p.X, p.Y-1)]); }
                    
            }
        }

        public bool FindPath(int sx, int sy, int tx, int ty) 
        {
            int sNode = NodeMap[new Point(sx, sy)];
            int tNode = NodeMap[new Point(tx, ty)];

            return FindPath(sNode, tNode);
        }
        
        /// <summary>
        /// Breadth first search
        /// </summary>
        /// <param name="s">source</param>
        /// <param name="t">target</param>
        /// <returns></returns>
        public bool FindPath(int s, int t)
        {
            Queue<int> queue = new Queue<int>();
            bool[] visited = new bool[NodeList.Count];

            queue.Enqueue(s);
            visited[s] = true;
            
            while (queue.Count != 0)
            {
                int x = queue.Dequeue();
                Node xNode = NodeList.Find(node => node.Index == x); 
                int N = xNode.Neighbors.Count;
                
                if (x == t) return true;
                for (int i = 0; i < N; i++)
                {
                    Node v = xNode.Neighbors[i];
                    if (!visited[v.Index])
                    { queue.Enqueue(v.Index); visited[v.Index] = true; }
                }
            }
            return false;
        }

        public void Draw()
        {
            for (var i = 0; i < this.NodeList.Count; i++)
            {
                var head = this.NodeList[i];
                var stringBuilder = new StringBuilder();
                stringBuilder.Append(i);
                stringBuilder.Append(" -> ");

                foreach (Node n in head.Neighbors)
                {
                    stringBuilder.Append(" , ");
                    stringBuilder.Append(n.Index);
                }

                Console.WriteLine(stringBuilder.ToString());
            }
        }

        private Dictionary<Point, int> NodeMap;
        private Dictionary<int, Point> RevNodeMap;

        /// <summary>
        /// Can we go up in the graph (i.e. grid or city map)?
        /// </summary>
        /// <param name="cityTile">city map (only the "roads"</param>
        /// <param name="p">current position in the city</param>
        /// <returns>true if possible</returns>
        private bool LookUp(CityBlock[,] cityTile, Point p)
        {
            if (p.Y != City.YMax - 1 && cityTile[p.X, p.Y + 1] == CityBlock.Vacant)
                return true;
            return false;
        }

        /// <summary>
        /// Can we go down in the graph (i.e. grid or city map)?
        /// </summary>
        /// <param name="cityTile">city map (only the "roads"</param>
        /// <param name="p">current position in the city</param>
        /// <returns>true if possible</returns>
        private bool LookDown(CityBlock[,] cityTile, Point p)
        {
            if (p.Y != 0 && cityTile[p.X, p.Y - 1] == CityBlock.Vacant)
                return true;
            return false;
        }

        /// <summary>
        /// Can we go right in the graph (i.e. grid or city map)?
        /// </summary>
        /// <param name="cityTile">city map (only the "roads"</param>
        /// <param name="p">current position in the city</param>
        /// <returns>true if possible</returns>
        private bool LookRight(CityBlock[,] cityTile, Point p)
        {
            if (p.X != City.XMax - 1 && cityTile[p.X+1, p.Y] == CityBlock.Vacant)
                return true;
            return false;
        }

        /// <summary>
        /// Can we go left in the graph (i.e. grid or city map)?
        /// </summary>
        /// <param name="cityTile">city map (only the "roads"</param>
        /// <param name="p">current position in the city</param>
        /// <returns>true if possible</returns>
        private bool LookLeft(CityBlock[,] cityTile, Point p)
        {
            if (p.X != 0 && cityTile[p.X-1, p.Y] == CityBlock.Vacant)
                return true;
            return false;
        }
    }

    public class Node
    {
        public List<Node> Neighbors;
        public int Index;

        public Node(int index)
        {
            Index = index;
            Neighbors = new List<Node>();
        }
        public void AddNeighbor(int index)
        {
            Neighbors.Add(new Node(index));
        }
    }
}
