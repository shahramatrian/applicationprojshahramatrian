﻿using System;

namespace TransportProj
{
    /// <summary>
    /// Abstract class to create cars
    /// </summary>
    public abstract class CarFactory
    {
        public abstract Car BuildCar(int xPos, int yPos, City city, Passenger passenger);
    }
}
